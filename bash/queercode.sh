#!/bin/bash

echo unsettling structures
awk 'BEGIN {
 while (structures -e power)
  print "destabilising " binary++
}'
